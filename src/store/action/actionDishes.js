import axios from "axios";

export const FETCH_DISHES_REQUEST = 'FETCH_DISHES_REQUEST';
export const FETCH_DISHES_SUCCESS = 'FETCH_DISHES_SUCCESS';
export const FETCH_DISHES_FAILURE = 'FETCH_DISHES_FAILURE';
export const FETCH_EDIT_DISHES_SUCCESS = 'FETCH_EDIT_DISHES_SUCCESS';
export const FETCH_DISH_SUCCESS = 'FETCH_DISH_SUCCESS';
export  const DEL_DISHES = 'DEL_DISHES';
export  const POST_DISHES = 'POST_DISHES';

export const fetchDishesRequest = () => ({type: FETCH_DISHES_REQUEST});
export const fetchDishesSuccess = dishes => ({type: FETCH_DISHES_SUCCESS, dishes});
export const fetchDishesFailure = error => ({type: FETCH_DISHES_FAILURE, error});
export const fetchDishSuccess = dish => ({type: FETCH_DISH_SUCCESS, dish});
export const delDishes = () => ({type: DEL_DISHES});
export const postDishes = () => ({type: POST_DISHES});


export const fetchDishes = () => {
    return async dispatch => {
            dispatch(fetchDishesRequest());
        try{
            const response = await axios.get('https://cafe-homework70-default-rtdb.firebaseio.com/dishes.json');
            const dishes = Object.keys(response.data).map(id => ({
                ...response.data[id],
                id
            }));
            dispatch(fetchDishesSuccess(dishes));
        } catch (error) {
            dispatch(fetchDishesFailure(error));
        }
    }
};

export const fetchInfoDishes = (id) => {
    return async dispatch => {
        try{
            dispatch(fetchDishesRequest());
            const response = await axios.get('https://cafe-homework70-default-rtdb.firebaseio.com/dishes/' + id +'.json');
            const dish = (response.data);
            dispatch(fetchDishSuccess(dish));
        } catch (error) {
            dispatch(fetchDishesFailure(error));
        }
    }
};

export const newDishesAdd= (input, props) => {
    return async dispatch => {
       try {
           await axios.post ('https://cafe-homework70-default-rtdb.firebaseio.com/dishes.json', input);
           dispatch(fetchDishes());
       } finally {
           props.history.push('/')
       }
    }
};

export const putDish = (id, data) => {
    return async dispatch => {
        try {
            await axios.put ('https://cafe-homework70-default-rtdb.firebaseio.com/dishes/' + id + '.json', data);
            dispatch(fetchDishes());
        } catch (e) {
            console.log(e);
        }
    }
};


export const deleteDishes = id => {
    return async dispatch => {
        try {
            await axios.delete('https://cafe-homework70-default-rtdb.firebaseio.com/dishes/' + id + '.json');
            dispatch(fetchDishes());
        } catch (error) {
            console.log(error);
        }
    };
};