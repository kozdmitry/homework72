import React, {useEffect} from 'react';
import "./Dishes.css";
import {Button, Grid} from "@material-ui/core";
import {NavLink} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {deleteDishes, fetchDishes} from "../../store/action/actionDishes";
import Dish from "../../components/Dish/Dish";

const Dishes = () => {

    const dispatch = useDispatch();
    const dishes = useSelector(state => state.dishes.dishes);

    useEffect(() => {
        dispatch(fetchDishes())
    }, [dispatch]);

    const delDishes = (e, id) => {
        e.preventDefault();
        dispatch(deleteDishes(id))
    };

    return (
        <Grid className="Dishes">
            <Grid container spacing={1}>
                <Grid item xs={9}>Dishes</Grid>
                <Grid item xs={3}>
                    <Button component={NavLink} to="/add" variant="contained" color="inherit">Add new Dish</Button>
                </Grid>

            </Grid>
            {dishes.map(dish => (
                <Dish
                    key={dish.id}
                    id={dish.id}
                    name={dish.name}
                    price={dish.price}
                    image={dish.image}
                    del={e => delDishes(e, dish.id)}
                />
            ))}

        </Grid>
    );
};

export default Dishes;