import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchOrders} from "../../store/action/actionOrders";
import Order from "../../components/Order/Order";
import {Grid} from "@material-ui/core";

const Orders = () => {

    const dispatch = useDispatch();
    const orders = useSelector(state => state.orders.orders);
    const dishes = useSelector(state => state.dishes.dishes);


    console.log(orders);

    useEffect(() => {
        dispatch(fetchOrders())
    }, [dispatch]);

    return (
        <Grid>
            {orders.map(orde => (
                <Order
                    key={orde.id}
                    id={orde.id}
                    name={orde.name}
                    price={orde.price}
                    image={orde.image}
                    // del={e => delDishes(e, dish.id)}
                />
            ))}
        </Grid>
    );
};

export default Orders;