import React, {useEffect, useState} from 'react';
import {fetchInfoDishes, putDish} from "../../store/action/actionDishes";
import {useDispatch, useSelector} from "react-redux";
import {Button, Grid, Paper, TextField} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import "./Edit.css";

const Edit = ({match}) => {
    const dispatch = useDispatch();
    const oneDish = useSelector(state => state.dishes.dish);

    const [oneInfo, setOneInfo] = useState({
        name: '',
        price: '',
        image: ''
    })

    useEffect(() => {
        dispatch(fetchInfoDishes(match.params.id))
    }, [dispatch, match.params.id]);

    useEffect(() => {
        setOneInfo(oneDish)
    }, [oneDish])

    const putDishHandler = (e) => {
        e.preventDefault();
        dispatch(putDish(match.params.id, oneInfo));
    };


    const changeInfo = event => {
        const {name, value} = event.target;

        setOneInfo(prev =>({
            ...prev,
            [name]: value
        }));
    };


    return (
        <Paper>
            <Typography/>
            <form className="form" onSubmit={e => putDishHandler(e)}>
                <Grid container direction="column" spacing={2}>
                    <Typography />
                    <Grid item xs={3} spacing={2}>
                        <Paper spacing={3}>
                            <Typography variant="h6">Edit Dishes</Typography>
                        </Paper>
                    </Grid>
                    <Grid item xs={3}>
                        <Typography pb={5}>Menu</Typography>
                    </Grid>

                    <Grid item xs={6} spacing={2}>
                        <TextField
                            fullWidth
                            label="Dish"
                            name="name"
                            spacing={3}
                            variant="outlined"
                            value={oneInfo.name}
                            onChange={changeInfo}
                        >
                        </TextField>
                    </Grid>
                    <Grid item xs={6} spacing={2}>
                        <TextField
                            placeholder="Price"
                            name="price"
                            variant="outlined"
                            value={oneInfo.price}
                            onChange={changeInfo}
                        />
                    </Grid><Grid item xs={6} spacing={2}>
                    <TextField
                        placeholder="Img"
                        name="image"
                        variant="outlined"
                        value={oneInfo.image}
                        onChange={changeInfo}
                    />
                </Grid>
                    <Grid item>
                        <Button type="submit" variant="outlined" color="primary">Edit</Button>
                    </Grid>
                </Grid>
            </form>
        </Paper>
    );
};

export default Edit;