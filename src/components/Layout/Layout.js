import React from 'react';
import "./Layout.css";
import {AppBar, Button, CssBaseline, Grid, Toolbar, Typography} from "@material-ui/core";
import {NavLink} from "react-router-dom";

const Layout = ({children}) => {
    return (
        <>
            <CssBaseline />
            <AppBar position="fixed">
                <Toolbar>
                    <Grid container justify="space-between">
                        <Grid item>
                            <Typography variant="h6" noWrap>
                                Turtle Pizza Admin
                            </Typography>
                        </Grid>
                        <Grid item>
                            <Button component={NavLink} to="/" color="inherit">DISHES</Button>
                            <Button component={NavLink} to="/orders" color="inherit">ORDERS</Button>
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
            <main>
                {children}
            </main>

        </>
    );
};

export default Layout;