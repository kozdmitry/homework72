import React from 'react';
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core";
import {NavLink} from "react-router-dom";

const useStyle = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px',
        marginBottom: '30px'
    },
    contentBlock: {
        display: 'flex',
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        marginBottom: '20px'
    }
}));

const Dish = ({image, name, price, id ,del}) => {
    const classes = useStyle();

    return (
        <Card className={classes.root}>
            <div className={classes.contentBlock}>
                <CardMedia
                    className={classes.cover}
                    image={image}
                    title="Live from space album cover"
                />
                <CardContent className={classes.content}>
                    <Typography component="h6" variant="h6" className={classes.title}>
                        {name}
                    </Typography>
                    <Typography color="textSecondary">
                        <strong>{price} </strong> KGS
                    </Typography>
                </CardContent>
            </div>
            <div>
                <Button component={NavLink} to={"/edit/" + id} variant="contained" color="secondary">Edit</Button>
                <Button onClick={del} variant="contained" color="primary">Del</Button>
            </div>
        </Card>
    );
};

export default Dish;