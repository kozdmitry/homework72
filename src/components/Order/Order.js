import React from 'react';
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";

const useStyle = makeStyles(() => ({
    root: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: '10px',
        marginBottom: '30px',
        marginTop: '100px'
    },
    contentBlock: {
        display: 'flex',
    },
    cover: {
        width: '150px',
        height: '150px',
        overflow: 'hidden',
        borderRadius: '4px'
    },
    content: {
        display: 'flex',
        flexDirection: 'column'
    },
    title: {
        marginBottom: '20px'
    }
}));
const Order = ({image, name, price, id ,del}) => {
    const classes = useStyle();

    return (
        <Card className={classes.root}>
            <div className={classes.contentBlock}>
                <CardMedia
                    className={classes.cover}
                    image={image}
                />
                <CardContent className={classes.content}>
                    <Typography component="h6" variant="h6" className={classes.title}>
                        {name}
                    </Typography>
                    <Typography color="textSecondary">
                        <strong>{price} </strong>
                        <strong>Delivery : 150 KGZ</strong>
                    </Typography>
                </CardContent>
            </div>
        </Card>
    );
};

export default Order;