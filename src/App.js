import React from "react";
import {Route, Switch} from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Dishes from "./container/Dishes/Dishes";
import Orders from "./container/Orders/Orders";
import AddPage from "./container/AddPage/AddPage";
import Edit from "./container/EditPages/Edit";

const App =() => (
    <Layout>
        <Switch>
            <Route path="/edit/:id" component={Edit} />
            <Route path="/add" component={AddPage} />
            <Route path="/" exact component={Dishes} />
            <Route path="/orders" component={Orders} />
        </Switch>
    </Layout>

);

export default App;
